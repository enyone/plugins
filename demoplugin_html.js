/*
**  J2Chrono Demo Plugin v1.1
**  JavaScript engine
**
**  HTML print example
**
**  Enymind Production (C) 2010
**  http://www.enymind.fi/
*/


/* Import needed packages */
importPackage( java.util );

/* Static variable defines */
var PluginType_Track = 1;
var PluginType_Route = 2;
var PluginType_Both = 3;
var PluginType_Http = 4;

var PluginOutput_Html = 1;
var PluginOutput_List = 2;


/* Get short presentation of plugin's name */
function getName()
{
  return "Demo Plugin";
}

/* Get plugin's version number */
function getVersion()
{
  return 1.1;
}

/* Get long presentation of plugin's name */
function getFullName()
{
  return "Demo Plugin v" + getVersion();
}

/* Default implementation of method toString */
function toString()
{
  return getFullName();
}

/* Get plugin type */
function getType()
{
  return PluginType_Track;
}

/* Get plugin output format */
function getOutputFormat()
{
  return PluginOutput_Html;
}

/* Get possible options displayed at plugin window combobox */
function getOptionGroup()
{
  return Arrays.asList( [ 'Select 1', 'Select 2', 'Last option' ] );
}

/* Get list column names when using PluginOutput_List */
function getColumns()
{
  return Arrays.asList( [] );
}

/* Get information about data what we need with next invoke (fireDataUpdate) */
/* See Plugin API for available methods */
function getInvokeWith()
{
  return Arrays.asList( [ 'getPassings' ] );
}

/* Invoke event when competitor is selected in list when using PluginOutput_List */
function fireCompetitorSelection( competitors )
{
  // Nothing here..
}

/* Invoke function (data processing itself) */
function fireDataUpdate( invokeData, selectedOptionIndex )
{
  // print some title
  var returnHtml = "Passing times:<br /><br />";
  
  var invokes = invokeData.toArray();
  var passings = invokes[ getInvokeWith().indexOf("getPassings") ].toArray();
  
  for( var i=0; i < passings.length; i++ )
  {
    passing = passings[i].toArray();
    
    // time from column index 3 (see Plugin API for column indexes)
    returnHtml += "" + passing[ 3 ] + "<br />";
  }
  
  // print selected option
  try
  {
    returnHtml += "<br />Selected option: " + selectedOptionIndex + " which is: " + getOptionGroup().get( selectedOptionIndex );
  }
  catch( error )
  {
    returnHtml += "<br />Selected option: " + selectedOptionIndex + " which does not exist";
  }
  
  return returnHtml;
}

