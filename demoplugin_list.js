/*
**  J2Chrono Demo Plugin v1.1
**  JavaScript engine
**
**  LIST example
**
**  Enymind Production (C) 2010
**  http://www.enymind.fi/
*/


/* Import needed packages */
importPackage( java.util );

/* Static variable defines */
var PluginType_Track = 1;
var PluginType_Route = 2;
var PluginType_Both = 3;
var PluginType_Http = 4;

var PluginOutput_Html = 1;
var PluginOutput_List = 2;


/* Get short presentation of plugin's name */
function getName()
{
  return "Demo Plugin Other";
}

/* Get plugin's version number */
function getVersion()
{
  return 1.1;
}

/* Get long presentation of plugin's name */
function getFullName()
{
  return "Demo Plugin v" + getVersion();
}

/* Default implementation of method toString */
function toString()
{
  return getFullName();
}

/* Get plugin type */
function getType()
{
  return PluginType_Track;
}

/* Get plugin output format */
function getOutputFormat()
{
  return PluginOutput_List;
}

/* Get possible options displayed at plugin window combobox */
function getOptionGroup()
{
  return Arrays.asList( [ 'Select 1', 'Select 2', 'Last option' ] );
}

/* Get list column names when using PluginOutput_List */
function getColumns()
{
  return Arrays.asList( [ 'Column 1', 'Column 2', 'Other column' ] );
}

/* Get information about data what we need with next invoke (fireDataUpdate) */
/* See Plugin API for available methods */
function getInvokeWith()
{
  return Arrays.asList( [ 'getPassings' ] );
}

/* Invoke event when competitor is selected in list when using PluginOutput_List */
function fireCompetitorSelection( competitors )
{
  // Nothing here..
}

/* Invoke function (data processing itself) */
function fireDataUpdate( invokeData, selectedOptionIndex )
{
  var invokes = invokeData.toArray();
  var passings = invokes[ getInvokeWith().indexOf("getPassings") ].toArray();
  
  // Create an empty array
  var returnData = new ArrayList();
  
  var counter = 1;
  
  for( var i=0; i < passings.length; i++ )
  {
    passing = passings[i].toArray();
    
    // Create row data with 3 columns
    var row = new ArrayList();
    row.add( "Row " + counter + " data" );
    row.add( passing[ 3 ] ); // time from column index 3 (see Plugin API for column indexes)
    row.add( "Selected: " + selectedOptionIndex );
    
    // Add row to array
    returnData.add( row );
    
    // Increase row counter
    counter ++;
  }
  
  return returnData;
}

