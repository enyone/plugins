/*
**  Team Result Layout per Class
**  JavaScript engine
*/


/* Import needed packages */
importPackage( java.util );

/* Static variable defines */
var PluginType_Track = 1;
var PluginType_Route = 2;
var PluginType_Both = 3;
var PluginType_Http = 4;

var PluginOutput_Html = 1;
var PluginOutput_List = 2;


/* Get short presentation of plugin's name */
function getName()
{
  return "Team Result Layout per Class";
}

/* Get plugin's version number */
function getVersion()
{
  return 1.0;
}

/* Get long presentation of plugin's name */
function getFullName()
{
  return getName() + " v" + getVersion();
}

/* Default implementation of method toString */
function toString()
{
  return getFullName();
}

/* Get plugin type */
function getType()
{
  //return PluginType_Track;
  //return PluginType_Route;
  return PluginType_Both;
  //return PluginType_Http;
}

/* Get plugin output format */
function getOutputFormat()
{
  //return PluginOutput_List;
  return PluginOutput_Html;
}

/* Get possible options displayed at plugin window combobox */
function getOptionGroup()
{
  return Arrays.asList( [ "A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3", "U20", "V40", "V50" ] );
}

/* Get list column names when using PluginOutput_List */
function getColumns()
{
  return Arrays.asList( [ ] );
}

/* Get information about data what we need with next invoke (fireDataUpdate) */
/* See Plugin API for available methods */
function getInvokeWith()
{
  return Arrays.asList( [ 'getRoundLaptimes', 'getRoundsPoints' ] );
}

/* Invoke event when competitor is selected in list when using PluginOutput_List */
function fireCompetitorSelection( competitors )
{
  //
}

function StringToTime( string )
{
  var negative = false;
  if( string.charAt( 0 ) == '-' )
    negative = true;

  if( string == null || string == "00:00:00.000" )
    return 0;
  else if( string.match("^([0-2]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(\\.)([0-9]{3})$") ||
           string.match("^\\-([0-2]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(\\.)([0-9]{3})$")
         )
  {
    var pString = string.split(":");
    var sString = pString[2].split("\\.");

    var sum = ( pString[0] * 3600 * 1000 );
    sum += ( pString[1] * 60 * 1000 );
    sum += ( sString[0] * 1000 );
    sum += ( sString[1] * 1 );

    if( negative )
      sum = -sum;

    return sum;
  }
  else
    return 0;
}

function TimeToString( time )
{
  var negative = false;
  if( time < 0 )
  {
    negative = true;
    time = -time;
  }

  var hours = ( time / ( 3600 * 1000 ) );
  var minutes = ( time % ( 3600 * 1000 ) ) / ( 60 * 1000 );
  var seconds = ( ( time % ( 3600 * 1000 ) ) % ( 60 * 1000 ) ) / 1000;
  var sSeconds = ( ( time % ( 3600 * 1000 ) ) % ( 60 * 1000 ) ) % 1000;

  var tHours = ( Math.round( Math.floor( hours ) ) ) + "";
  var tMinutes = ( Math.round( Math.floor( minutes ) ) ) + "";
  var tSeconds = ( Math.round( Math.floor( seconds ) ) ) + "";
  var tSSeconds = ( Math.round( Math.floor( sSeconds ) ) ) + "";

  if( hours < 10 )
    tHours = "0" + tHours;
  if( minutes < 10 )
    tMinutes = "0" + tMinutes;
  if( seconds < 10 )
    tSeconds = "0" + tSeconds;

  if( sSeconds < 10 )
    tSSeconds = "00" + tSSeconds;
  else if( sSeconds < 100 )
    tSSeconds = "0" + tSSeconds;

  return ( ( negative ) ? "-" : "" ) + tHours + ":" + tMinutes + ":" + tSeconds + "." + tSSeconds;
}

function ToAT(secsStr)
{
  var secs = parseInt(secsStr);
  return "+" + TimeToString(secs*1000).substring(3, 8);
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/* Invoke function (data processing itself) */
function fireDataUpdate( invokeData, selectedOptionIndex )
{
  var invokes = invokeData.toArray();

  var laptimes = invokes[ getInvokeWith().indexOf("getRoundLaptimes") ].toArray();
  var roundsPoints = invokes[ getInvokeWith().indexOf("getRoundsPoints") ].toArray();

  var html = "";
  html += "<table style=\"width: 100%; font-family: sans-serif; font-size: 14px;\">\n";

  html += "<tr>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\"></td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Joukkue</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Jäsenet</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Luokka</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Lisätietoja</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Yhteensä</td>";
  html += "</tr>\n";

  var gaps = [];
  var counts = [];
  var teamTimes = [];
  var teamNames = [];
  var teamGots = [];
  var teamGots2 = [];

  var old = "";
  var put = "";

  for( var i=0; i < laptimes.length; i++ )
  {
    enduroGapTime = laptimes[i].toArray();

    old = "";
    if( gaps[ enduroGapTime[2] ] ) // enduroGapTime [2]: number
      old = gaps[ enduroGapTime[2] ];

    if( counts[ enduroGapTime[2] ] )
      counts[ enduroGapTime[2] ] = counts[ enduroGapTime[2] ] + 1;
    else
      counts[ enduroGapTime[2] ] = 0;

    put = "";
    if( counts[ enduroGapTime[2] ] > 5 )
    {
      counts[ enduroGapTime[2] ] = 0;
      put += "</tr><tr style='color: gray; text-decoration: italic;'>";
    }

    put += old + "<td style=\"border-left: 1px solid silver; border-bottom: 1px solid gray;\">&nbsp;" + enduroGapTime[7] + "&nbsp;</td>"; // enduroGapTime [7]: gap

    gaps[ enduroGapTime[2] ] = put;
  }

  for( var i=0; i < roundsPoints.length; i++ )
  {
    roundsPoint = roundsPoints[i].toArray();
    teamTemp = roundsPoint[0].split(" ")[0];
    
    if(isNumeric(teamTemp))
      team = teamTemp;
    else
      team = 0;
      
    if(teamNames[team] == undefined)
      teamNames[team] = roundsPoint[0]; // time
    else
      teamNames[team] = teamNames[team] + "<br/>" + roundsPoint[0]; // time
  }

  var rowCounter = 0;
  var previousTime = "00:00:00.000";

  for( var i=0; i < roundsPoints.length; i++ )
  {
    roundsPoint = roundsPoints[i].toArray();
    teamTemp = roundsPoint[0].split(" ")[0];
    
    if(isNumeric(teamTemp))
      team = teamTemp;
    else
      team = 0;
    
    if(teamGots[team] == undefined)
    {
      teamGots[team] = true;
      continue;
    }
    
    if(teamGots2[team] != undefined)
      continue;
    
    teamGots2[team] = true;

    if( selectedOptionIndex == -1 || getOptionGroup().get( selectedOptionIndex ).equals( roundsPoint[4] ) ) // skip class
    {
        rowCounter ++;
        var diff = TimeToString( StringToTime( roundsPoint[7] ) - StringToTime( previousTime ) ); // roundsPoints [7]: total_time

        if( diff == roundsPoint[7] )
            diff = "-";
        else if( diff.indexOf( "-" ) > -1 )
            diff = "-";

        html += "<tr>";
        var at = "-";

        if( roundsPoint[6] == "" ) // roundsPoints [6]: penalties
            html += "<td style=\"border-bottom: 1px solid silver;\">" + rowCounter + ".</td>";
        else if( roundsPoint[6].indexOf("sec") > 0 )
        {
            at = ToAT(roundsPoint[6].replace("+", "").replace("sec", ""));
            html += "<td style=\"border-bottom: 1px solid silver;\">" + rowCounter + ".</td>";
        }
        else
            html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[6] + "</td>";

        html += "<td style=\"border-bottom: 1px solid silver;\">" + team + "</td>";
        html += "<td style=\"border-bottom: 1px solid silver;\">" + teamNames[team].replace(/^[0-9]+/g, roundsPoint[2] + ".").replace(/>[0-9]+\ /g, ">") + "</td>"; // roundsPoints [0]: name // roundsPoints [2]: number
        html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[4] + "</td>"; // roundsPoints [4]: class
        html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[1] + "</td>"; // roundsPoints [1]: additional
        html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[7] + "</td>"; // time
        html += "</tr>\n";

        if( gaps[ roundsPoint[2] ] )
        {
            html += "<tr style='color: gray; text-decoration: italic;'>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td colspan='6'><table style=\"font-family: sans-serif;\"><tr>";
            html += gaps[ roundsPoint[2] ];
            html += "</tr></table></td>";
            html += "</tr>\n";
        }

        previousTime = roundsPoint[7];
    }
  }

  html += "</table>\n";

  return html;
}
