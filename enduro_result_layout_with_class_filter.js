/*
**  Enduro Result Layout
**  JavaScript engine
*/


/* Import needed packages */
importPackage( java.util );

/* Static variable defines */
var PluginType_Track = 1;
var PluginType_Route = 2;
var PluginType_Both = 3;
var PluginType_Http = 4;

var PluginOutput_Html = 1;
var PluginOutput_List = 2;


/* Get short presentation of plugin's name */
function getName()
{
  return "Enduro Result Layout with class filter";
}

/* Get plugin's version number */
function getVersion()
{
  return 1.0;
}

/* Get long presentation of plugin's name */
function getFullName()
{
  return getName() + " v" + getVersion();
}

/* Default implementation of method toString */
function toString()
{
  return getFullName();
}

/* Get plugin type */
function getType()
{
  //return PluginType_Track;
  return PluginType_Route;
  //return PluginType_Both;
  //return PluginType_Http;
}

/* Get plugin output format */
function getOutputFormat()
{
  //return PluginOutput_List;
  return PluginOutput_Html;
}

/* Get possible options displayed at plugin window combobox */
function getOptionGroup()
{
  return Arrays.asList( [ 'Ungdom', 'C', 'Damer', 'Veteran', 'Bredde', 'Junior Rekrutt', 'Senior', 'Elite', 'Junior Elite', 'Junior' ] );
}

/* Get list column names when using PluginOutput_List */
function getColumns()
{
  return Arrays.asList( [ ] );
}

/* Get information about data what we need with next invoke (fireDataUpdate) */
/* See Plugin API for available methods */
function getInvokeWith()
{
  return Arrays.asList( [ 'getEnduroGapTimes', 'getRoundsPoints' ] );
}

/* Invoke event when competitor is selected in list when using PluginOutput_List */
function fireCompetitorSelection( competitors )
{
  //
}

function StringToTime( string )
{
  var negative = false;
  if( string.charAt( 0 ) == '-' )
    negative = true;

  if( string == null || string == "00:00:00.000" )
    return 0;
  else if( string.match("^([0-2]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(\\.)([0-9]{3})$") ||
           string.match("^\\-([0-2]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(\\.)([0-9]{3})$")
         )
  {
    var pString = string.split(":");
    var sString = pString[2].split("\\.");

    var sum = ( pString[0] * 3600 * 1000 );
    sum += ( pString[1] * 60 * 1000 );
    sum += ( sString[0] * 1000 );
    sum += ( sString[1] * 1 );

    if( negative )
      sum = -sum;

    return sum;
  }
  else
    return 0;
}

function TimeToString( time )
{
  var negative = false;
  if( time < 0 )
  {
    negative = true;
    time = -time;
  }

  var hours = ( time / ( 3600 * 1000 ) );
  var minutes = ( time % ( 3600 * 1000 ) ) / ( 60 * 1000 );
  var seconds = ( ( time % ( 3600 * 1000 ) ) % ( 60 * 1000 ) ) / 1000;
  var sSeconds = ( ( time % ( 3600 * 1000 ) ) % ( 60 * 1000 ) ) % 1000;

  var tHours = ( Math.round( Math.floor( hours ) ) ) + "";
  var tMinutes = ( Math.round( Math.floor( minutes ) ) ) + "";
  var tSeconds = ( Math.round( Math.floor( seconds ) ) ) + "";
  var tSSeconds = ( Math.round( Math.floor( sSeconds ) ) ) + "";

  if( hours < 10 )
    tHours = "0" + tHours;
  if( minutes < 10 )
    tMinutes = "0" + tMinutes;
  if( seconds < 10 )
    tSeconds = "0" + tSeconds;

  if( sSeconds < 10 )
    tSSeconds = "00" + tSSeconds;
  else if( sSeconds < 100 )
    tSSeconds = "0" + tSSeconds;

  return ( ( negative ) ? "-" : "" ) + tHours + ":" + tMinutes + ":" + tSeconds + "." + tSSeconds;
}

function ToAT(secsStr)
{
  var secs = parseInt(secsStr);
  return "+" + TimeToString(secs*1000).substring(3, 8);
}

/* Invoke function (data processing itself) */
function fireDataUpdate( invokeData, selectedOptionIndex )
{
  var invokes = invokeData.toArray();

  var enduroGapTimes = invokes[ getInvokeWith().indexOf("getEnduroGapTimes") ].toArray();
  var roundsPoints = invokes[ getInvokeWith().indexOf("getRoundsPoints") ].toArray();

  var html = "";
  html += "<table style=\"width: 100%; font-family: sans-serif; font-size: 14px;\">\n";

  html += "<tr>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Pos</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">No</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Name</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Class</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Information</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Total</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">Gap</td>";
  html += "<td style=\"border-bottom: 1px solid gray; padding-right: 15px; background-color: #F5F5F5; font-weight: bold;\">TC penalties</td>";
  html += "</tr>\n";

  var gaps = [];
  var counts = [];

  var old = "";
  var put = "";

  for( var i=0; i < enduroGapTimes.length; i++ )
  {
    enduroGapTime = enduroGapTimes[i].toArray();

    old = "";
    if( gaps[ enduroGapTime[2] ] ) // enduroGapTime [2]: number
      old = gaps[ enduroGapTime[2] ];

    if( counts[ enduroGapTime[2] ] )
      counts[ enduroGapTime[2] ] = counts[ enduroGapTime[2] ] + 1;
    else
      counts[ enduroGapTime[2] ] = 0;

    put = "";
    if( counts[ enduroGapTime[2] ] > 5 )
    {
      counts[ enduroGapTime[2] ] = 0;
      put += "</tr><tr style='color: gray; text-decoration: italic;'>";
    }

    put += old + "<td style=\"border-left: 1px solid silver; border-bottom: 2px solid gray;\">&nbsp;ST" + enduroGapTime[6] + " " + enduroGapTime[7] + "&nbsp;</td>"; // enduroGapTime [7]: gap

    gaps[ enduroGapTime[2] ] = put;
  }

  var rowCounter = 0;
  var previousTime = "00:00:00.000";

  for( var i=0; i < roundsPoints.length; i++ )
  {
    roundsPoint = roundsPoints[i].toArray();

    if( selectedOptionIndex == -1 || getOptionGroup().get( selectedOptionIndex ).equals( roundsPoint[4] ) ) // skip class
    {
        rowCounter ++;
        var diff = TimeToString( StringToTime( roundsPoint[7] ) - StringToTime( previousTime ) ); // roundsPoints [7]: total_time

        if( diff == roundsPoint[7] )
            diff = "-";
        else if( diff.indexOf( "-" ) > -1 )
            diff = "-";

        html += "<tr>";
        var at = "-";

        if( roundsPoint[6] == "" ) // roundsPoints [6]: penalties
            html += "<td style=\"border-bottom: 1px solid silver;\">" + rowCounter + ".</td>";
        else if( roundsPoint[6].indexOf("sec") > 0 )
        {
            at = ToAT(roundsPoint[6].replace("+", "").replace("sec", ""));
            html += "<td style=\"border-bottom: 1px solid silver;\">" + rowCounter + ".</td>";
        }
        else
            html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[6] + "</td>";

        html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[2] + "</td>"; // roundsPoints [2]: number
        html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[0] + "</td>"; // roundsPoints [0]: name
        html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[4] + "</td>"; // roundsPoints [4]: class
        html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[1] + "</td>"; // roundsPoints [1]: additional
        html += "<td style=\"border-bottom: 1px solid silver;\">" + roundsPoint[7] + "</td>";
        html += "<td style=\"border-bottom: 1px solid silver;\">" + diff + "</td>";
        html += "<td style=\"border-bottom: 1px solid silver;\">" + at + "</td>"; // roundsPoints [9]: total
        html += "</tr>\n";

        if( gaps[ roundsPoint[2] ] )
        {
            html += "<tr style='color: gray; text-decoration: italic;'>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td colspan='6'><table style=\"font-family: sans-serif;\"><tr>";
            html += gaps[ roundsPoint[2] ];
            html += "</tr></table></td>";
            html += "</tr>\n";
        }

        previousTime = roundsPoint[7];
    }
  }

  html += "</table>\n";

  return html;
}
