Plugins
===========

For plugins used in **J2Chrono** application.

Branch definitions
-----------

* **master** branch is locked branch where only selected users are able to commit changes
* **partner_dev** branch is open branch where everyone are able to commit 1) changes

1) Special notes for committing
-----------

Before you commit any changes to **partner_dev** branch make sure that:

* you are aware that content of this repository and all branches inside it are publicly available to the Internet
* you must mark the content of your commit to be licensed under some open source license (like MIT or Apache)
* you must use related commit message and description of the changes you commit
* you must use your own GitLab account for that commit

By committing to **partner_dev** branch you accept terms mentioned above.