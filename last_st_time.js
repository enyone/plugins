/*
**  J2Chrono Demo Plugin v1.1
**  JavaScript engine
**
**  HTML print example
**
**  Enymind Production (C) 2015
**  http://www.enymind.fi/
*/


/* Import needed packages */
importPackage( java.util );

/* Static variable defines */
var PluginType_Track = 1;
var PluginType_Route = 2;
var PluginType_Both = 3;
var PluginType_Http = 4;

var PluginOutput_Html = 1;
var PluginOutput_List = 2;


/* Get short presentation of plugin's name */
function getName()
{
  return "Last ST time";
}

/* Get plugin's version number */
function getVersion()
{
  return 1.1;
}

/* Get long presentation of plugin's name */
function getFullName()
{
  return "Last ST time " + getVersion();
}

/* Default implementation of method toString */
function toString()
{
  return getFullName();
}

/* Get plugin type */
function getType()
{
  return PluginType_Http;
}

/* Get plugin output format */
function getOutputFormat()
{
  return PluginOutput_Html;
}

/* Get possible options displayed at plugin window combobox */
function getOptionGroup()
{
  return Arrays.asList( [] );
}

/* Get list column names when using PluginOutput_List */
function getColumns()
{
  return Arrays.asList( [] );
}

/* Get information about data what we need with next invoke (fireDataUpdate) */
/* See Plugin API for available methods */
function getInvokeWith()
{
  return Arrays.asList( [ 'getLatestTimeForEveryGap' ] );
}

/* Invoke event when competitor is selected in list when using PluginOutput_List */
function fireCompetitorSelection( competitors )
{
  // Nothing here..
}

/* Invoke function (data processing itself) */
function fireDataUpdate( invokeData, selectedOptionIndex )
{
  var returnHtml = "<div style='text-align: center;'>";
  
  var invokes = invokeData.toArray();
  var passings = invokes[ getInvokeWith().indexOf("getLatestTimeForEveryGap") ].toArray();
  
  var maxST = 0;
  for( var i=0; i < passings.length; i++ )
  {
    passing = passings[i].toArray();
    
    if( maxST < parseInt( passing[ 6 ] ) )
      maxST = parseInt( passing[ 6 ] );
  }
  
  for( var i=0; i < passings.length; i++ )
  {
    passing = passings[i].toArray();
    
    if( parseInt( passing[ 6 ] ) == maxST )
    {
      returnHtml += "<h1 style='font-size: 6em; font-family: monospace;'>" + passing[ 2 ] + " " + passing[ 0 ] + "</h1><br />";
      returnHtml += "<h1 style='font-size: 10em; font-family: monospace; font-weight: bold;'>" + passing[ 7 ] + "</h1><br />";
      returnHtml += "<h1 style='font-size: 4em; font-family: monospace; font-weight: bold;'>" + passing[ 8 ] + "</h1><br />";
    }
  }
  
  returnHtml += "</div>";
  
  return returnHtml;
}
