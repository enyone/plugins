/*
**  An empty J2Chrono plugin
**  JavaScript engine
*/


/* Import needed packages */
importPackage( java.util );

/* Static variable defines */
var PluginType_Track = 1;
var PluginType_Route = 2;
var PluginType_Both = 3;
var PluginType_Http = 4;

var PluginOutput_Html = 1;
var PluginOutput_List = 2;


/* Get short presentation of plugin's name */
function getName()
{
  return "No name";
}

/* Get plugin's version number */
function getVersion()
{
  return 1.0;
}

/* Get long presentation of plugin's name */
function getFullName()
{
  return getName() + " v" + getVersion();
}

/* Default implementation of method toString */
function toString()
{
  return getFullName();
}

/* Get plugin type */
function getType()
{
  return PluginType_Track;
  //return PluginType_Route;
  //return PluginType_Both;
  //return PluginType_Http;
}

/* Get plugin output format */
function getOutputFormat()
{
  return PluginOutput_List;
  //return PluginOutput_Html;
}

/* Get possible options displayed at plugin window combobox */
function getOptionGroup()
{
  return Arrays.asList( [ ] );
}

/* Get list column names when using PluginOutput_List */
function getColumns()
{
  return Arrays.asList( [ ] );
}

/* Get information about data what we need with next invoke (fireDataUpdate) */
/* See Plugin API for available methods */
function getInvokeWith()
{
  return Arrays.asList( [ ] );
}

/* Invoke event when competitor is selected in list when using PluginOutput_List */
function fireCompetitorSelection( competitors )
{
  //
}

/* Invoke function (data processing itself) */
function fireDataUpdate( invokeData, selectedOptionIndex )
{
  var invokes = invokeData.toArray();
  
  // An empty array
  var returnData = new ArrayList();
  
  return returnData;
}

