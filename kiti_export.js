/*
**  Enduro Result Layout
**  JavaScript engine
*/


/* Import needed packages */
importPackage( java.util );

/* Static variable defines */
var PluginType_Track = 1;
var PluginType_Route = 2;
var PluginType_Both = 3;
var PluginType_Http = 4;

var PluginOutput_Html = 1;
var PluginOutput_List = 2;


/* Get short presentation of plugin's name */
function getName()
{
  return "Kiti Export";
}

/* Get plugin's version number */
function getVersion()
{
  return 1.0;
}

/* Get long presentation of plugin's name */
function getFullName()
{
  return getName() + " v" + getVersion();
}

/* Default implementation of method toString */
function toString()
{
  return getFullName();
}

/* Get plugin type */
function getType()
{
  //return PluginType_Track;
  return PluginType_Route;
  //return PluginType_Both;
  //return PluginType_Http;
}

/* Get plugin output format */
function getOutputFormat()
{
  return PluginOutput_List;
  //return PluginOutput_Html;
}

/* Get possible options displayed at plugin window combobox */
function getOptionGroup()
{
  return Arrays.asList( [ ] );
}

/* Get list column names when using PluginOutput_List */
function getColumns()
{
  return Arrays.asList( [ 'Lk','Nro','Kulj.nimi','II-ohjaaja','Lka sij','Kokon.sij.','Kokonaistulos (aika)','Selite','Syy' ] );
}

/* Get information about data what we need with next invoke (fireDataUpdate) */
/* See Plugin API for available methods */
function getInvokeWith()
{
  return Arrays.asList( [ 'getCompetitionResults' ] );
}

/* Invoke event when competitor is selected in list when using PluginOutput_List */
function fireCompetitorSelection( competitors )
{
  //
}

function StringToTime( string )
{
  var negative = false;
  if( string.charAt( 0 ) == '-' )
    negative = true;

  if( string == null || string == "00:00:00.000" )
    return 0;
  else if( string.match("^([0-2]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(\\.)([0-9]{3})$") ||
           string.match("^\\-([0-2]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(:)([0-5]{1})([0-9]{1})(\\.)([0-9]{3})$")
         )
  {
    var pString = string.split(":");
    var sString = pString[2].split("\\.");

    var sum = ( pString[0] * 3600 * 1000 );
    sum += ( pString[1] * 60 * 1000 );
    sum += ( sString[0] * 1000 );
    sum += ( sString[1] * 1 );

    if( negative )
      sum = -sum;

    return sum;
  }
  else
    return 0;
}

function TimeToString( time )
{
  var negative = false;
  if( time < 0 )
  {
    negative = true;
    time = -time;
  }

  var hours = ( time / ( 3600 * 1000 ) );
  var minutes = ( time % ( 3600 * 1000 ) ) / ( 60 * 1000 );
  var seconds = ( ( time % ( 3600 * 1000 ) ) % ( 60 * 1000 ) ) / 1000;
  var sSeconds = ( ( time % ( 3600 * 1000 ) ) % ( 60 * 1000 ) ) % 1000;
  
  var tHours = ( Math.round( Math.floor( hours ) ) ) + "";
  var tMinutes = ( Math.round( Math.floor( minutes ) ) ) + "";
  var tSeconds = ( Math.round( Math.floor( seconds ) ) ) + "";
  var tSSeconds = ( Math.round( Math.floor( sSeconds ) ) ) + "";
  
  if( hours < 10 )
    tHours = "0" + tHours;
  if( minutes < 10 )
    tMinutes = "0" + tMinutes;
  if( seconds < 10 )
    tSeconds = "0" + tSeconds;
  
  if( sSeconds < 10 )
    tSSeconds = "00" + tSSeconds;
  else if( sSeconds < 100 )
    tSSeconds = "0" + tSSeconds;

  return ( ( negative ) ? "-" : "" ) + tHours + ":" + tMinutes + ":" + tSeconds + "." + tSSeconds;
}

if(!Array.indexOf){
	    Array.prototype.indexOf = function(obj){
	        for(var i=0; i<this.length; i++){
	            if(this[i]==obj){
	                return i;
	            }
	        }
	        return -1;
	    }
	}

/* Invoke function (data processing itself) */
function fireDataUpdate( invokeData, selectedOptionIndex )
{
  var invokes = invokeData.toArray();
  
  var enduroTimes = invokes[ getInvokeWith().indexOf("getCompetitionResults") ].toArray();
  
  var returnData = new ArrayList();
  
  var classes = new Array();
  var classCount = new Array();
  
  var counter = 1;
  var classCounter = 1;
  for( var i=0; i < enduroTimes.length; i++ )
  {
    enduroTime = enduroTimes[i].toArray();
    
    if( classes.indexOf( enduroTime[ 4 ] ) == -1 )
    {
      classes[ classCounter ] = enduroTime[ 4 ];
      classCounter ++;
    }
    
    var classNumber = classes.indexOf( enduroTime[ 4 ] );
    
    if( !classCount[ classNumber ] )
      classCount[ classNumber ] = 0;
      
    classCount[ classNumber ] ++;

    var row = new ArrayList();
    row.add( classNumber + "" );
    row.add( enduroTime[ 2 ] );
    row.add( enduroTime[ 0 ] );
    row.add( "" );
    row.add( classCount[ classNumber ] + "" );
    row.add( counter + "" );
    
    row.add( enduroTime[ 6 ].replace(".",",").replace(":",".") );
    
    if( enduroTime[ 8 ].indexOf( "+DNF" ) > -1 )
      row.add( "2" );
    else if( enduroTime[ 8 ].indexOf( "+DQ" ) > -1 )
      row.add( "3" );
    else
      row.add( "1" );
      
    row.add( enduroTime[ 8 ] );
    returnData.add( row );

    counter ++;
  }
  
  return returnData;
}
